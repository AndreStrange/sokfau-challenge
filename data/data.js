export const data=[

{
    question: 'Question 1',
    choices: [
        'Correct',
        'Wrong',
        'Wrong',
        'Wrong'
    ],
    answer: 'Correct'
},

{
    question: 'Question 2',
    choices: [
        'Wrong',
        'Correct',
        'Wrong',
        'Wrong'
    ],
    answer: 'Correct'
},

{
    question: 'Question 3',
    choices: [
        'Wrong',
        'Wrong',
        'Correct',
        'Wrong'
    ],
    answer: 'Correct'
},

{
    question: 'Question 4',
    choices: [
        'Wrong',
        'Wrong',
        'Wrong',
        'Correct'
    ],
    answer: 'Correct'
},

]